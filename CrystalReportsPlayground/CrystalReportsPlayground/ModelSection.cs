﻿
namespace CrystalReportsPlayground
{
    public class ModelSection
    {
        public string SubreportName { get; set; }

        public string SubreportReportPath { get; set; }

        public int SectionIndex { get; set; }

        public string ParamName { get; set; }
         
        public object ParamValue { get; set; }


        public string LocalName
        {
            get
            {
                return this.SubreportName + SectionIndex;
            }
        }

        public override string ToString()
        {
            return SubreportName;
        }
    }
}