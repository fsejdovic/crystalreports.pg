﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using CrystalDecisions.ReportAppServer.ClientDoc;
using CrystalDecisions.Shared;
using System.IO;

namespace CrystalReportsPlayground
{
    public partial class FrmMain : Form
    {
        private static readonly string kDetailSectionName = "Detail_";
        private static readonly string kHelperSectionSuffix = "_Helper";
        private static readonly string kMainReportFileName = "MainTest.rpt";

        private ReportLayoutConfig reportLayoutConfiguration = new ReportLayoutConfig();

        public FrmMain()
        {
            InitializeComponent();
        }

        private void BtnGenerateReport_Click(object sender, EventArgs e)
        {
            GenerateReport(reportLayoutConfiguration);
        }

        private string GetMainReportFullFilePath()
        {
            return Path.Combine(Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory())), kMainReportFileName);
        }

        private List<DataTable> FillMainDataSet()
        {
            List<DataTable> ldt = new List<DataTable>();
            DataTable dt = new DataTable("MainDataTable");
            dt.Columns.Add("FirstName");
            dt.Columns.Add("LastName");
            dt.Columns.Add("State");
            dt.Columns.Add("Town");
            dt.Columns.Add("Phone");
            dt.Columns.Add("ID");

            dt.Rows.Add(new object[] { "James", "Bond", "England", "London", "06311122233", "1233428749020" });
            dt.Rows.Add(new object[] { "Rocky", "Balboa", "USA", "Los Angeles", "06377788899", "471998425060" });
            dt.Rows.Add(new object[] { "Keyser", "Soze", "Hungary", "Budapest", "06355544433", "3643428747898" });

            ldt.Add(dt);
            return ldt;
        }

        private List<DataTable> FillSubReportDataSet()
        {
            List<DataTable> ldtsr = new List<DataTable>();

            DataTable dtsr = new DataTable("SectionDataTableTest1");
            dtsr.Columns.Add("SectionID");
            dtsr.Columns.Add("SectionName");

            dtsr.Rows.Add(new object[] { "1", "James Bond" });
            dtsr.Rows.Add(new object[] { "2", "Rocky Balboa" });
            dtsr.Rows.Add(new object[] { "3", "Keyser Sose" });

            ldtsr.Add(dtsr);
            return ldtsr;
        }

        public void GenerateReport(ReportLayoutConfig reportLayoutConfig)
        {
            CrystalDecisions.ReportAppServer.ReportDefModel.Area area;
            CrystalDecisions.ReportAppServer.ReportDefModel.Section section;

            if (reportLayoutConfiguration.ReportLayout == null)
            {
                MessageBox.Show("No subreports selected...");
                return;
            }
            
            CrystalDecisions.CrystalReports.Engine.ReportDocument document = null;

            try
            {
                document = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
                document.Load(GetMainReportFullFilePath());

                ISCDReportClientDocument reportClientDocument = document.ReportClientDocument;
                area = (CrystalDecisions.ReportAppServer.ReportDefModel.Area)reportClientDocument.ReportDefController.ReportDefinition.ReportFooterArea;

                int subReportCounter = 0;

                for (int counter = 0; counter < reportLayoutConfiguration.ReportLayout.Count; counter++)
                {
                    section = CreatePlaceholderDetailSection(document, counter);

                    reportClientDocument.ReportDefController.ReportSectionController.Add(section, area, -1);
                    reportClientDocument.SubreportController.ImportSubreport(reportLayoutConfiguration.ReportLayout[counter].LocalName, reportLayoutConfiguration.ReportLayout[counter].SubreportReportPath, section);

                    if (reportLayoutConfiguration.ReportLayout[counter].SubreportName == "SectionReportTest1")
                    {
                        List<DataTable> subReportDataSetSource = FillSubReportDataSet();
                        SetSubReportDataSource(document, subReportDataSetSource, subReportCounter);
                    }

                    subReportCounter++;
                }

                RemoveSubreportBorders(document);

                List<DataTable> ldt = FillMainDataSet();
                SetMainReportDataSource(document, ldt);

                List<ReportParameterList> reportParameterListFull = new List<ReportParameterList>();
                reportParameterListFull = SetReportParameters(reportLayoutConfig);

                foreach (var current in reportParameterListFull)
                {
                    if (string.IsNullOrEmpty(current.LocalName))
                    {
                        document.SetParameterValue(current.Name, current.Value);
                    }
                    else
                    {
                        document.SetParameterValue(current.Name, current.Value, current.LocalName);
                    }
                }
                document.ExportToDisk(ExportFormatType.PortableDocFormat, @"C:\output\ASD.pdf");
                crvReportViewer.ReportSource = document;
                crvReportViewer.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (document != null)
                {
                    document.Close();
                    document.Dispose();
                    document = null;
                }
            }
        }

        private CrystalDecisions.ReportAppServer.ReportDefModel.Section CreatePlaceholderDetailSection(CrystalDecisions.CrystalReports.Engine.ReportDocument document, int index, bool helperSection = false)
        {
            ISCDReportClientDocument reportClientDocument = document.ReportClientDocument;
            // get header section, to use as width measurement for new section to be added
            CrystalDecisions.ReportAppServer.ReportDefModel.Section headerSection = reportClientDocument.ReportDefController.ReportDefinition.PageHeaderArea.Sections[0];
            CrystalDecisions.ReportAppServer.ReportDefModel.Section resultSection = new CrystalDecisions.ReportAppServer.ReportDefModel.Section();

            //Set the properties for the section
            resultSection.Kind = CrystalDecisions.ReportAppServer.ReportDefModel.CrAreaSectionKindEnum.crAreaSectionKindReportFooter;
            // section must have width to be rendered correctly
            resultSection.Width = headerSection.Width;

            // section names must be unique
            if (helperSection)
            {
                resultSection.Name = kDetailSectionName + kHelperSectionSuffix;
                resultSection.Height = 0;
            }
            else
            {
                resultSection.Name = kDetailSectionName + index;
            }
            // section must have width to be rendered correctly
            resultSection.Width = headerSection.Width;
            return resultSection;
        }

        private List<ReportParameterList> SetReportParameters(ReportLayoutConfig reportLayoutConfig)
        {
            List<ReportParameterList> reportParameterList = new List<ReportParameterList>();
            //set subreport parameters
            for (int i = 0; i < reportLayoutConfig.ReportLayout.Count; i++)
            {
                reportParameterList.Add(new ReportParameterList { SubreportName = reportLayoutConfig.ReportLayout[i].SubreportName, LocalName = reportLayoutConfig.ReportLayout[i].LocalName, Name = reportLayoutConfig.ReportLayout[i].ParamName, Value = reportLayoutConfig.ReportLayout[i].ParamValue });
            }
            //set main report parameters
            reportParameterList.Add(new ReportParameterList { SubreportName = string.Empty, LocalName = string.Empty, Name = "Name", Value = "Main Report Parameter" });
            reportParameterList.Add(new ReportParameterList { SubreportName = string.Empty, LocalName = string.Empty, Name = "ID", Value = 123 });

            return reportParameterList;
        }

        private void SetMainReportDataSource(CrystalDecisions.CrystalReports.Engine.ReportDocument document, List<DataTable> tables)
        {
            if (tables.Count > 0)
            {
                DataSet reportDataSet = AddTableInReportDataSet(tables);
                document.SetDataSource(reportDataSet);
            }
        }

        private DataSet AddTableInReportDataSet(List<DataTable> tableList)
        {
            DataSet rv = new DataSet();
            tableList.ForEach(tbl => AddTableInReportDataSet(rv, tbl));
            return rv;
        }

        private void SetSubReportDataSource(CrystalDecisions.CrystalReports.Engine.ReportDocument document, List<DataTable> tables, int counter)
        {
            if (tables.Count > 0)
            {
                DataSet reportDataSet = AddTableInReportDataSet(tables);

                CrystalDecisions.CrystalReports.Engine.ReportDocument subReport = document.Subreports[counter];
                subReport.SetDataSource(reportDataSet);
            }
        }

        private void AddTableInReportDataSet(DataSet dataSet, DataTable table)
        {
            bool tableAlreadyAdded = dataSet.Tables.Cast<DataTable>().Where(tbl => tbl.TableName == table.TableName).Any();
            if (tableAlreadyAdded)
            {
                foreach (DataRow currentRow in table.Rows)
                {
                    dataSet.Tables[table.TableName].ImportRow(currentRow);
                }
            }
            else
            {
                dataSet.Tables.Add(table);
            }
        }

        private void RemoveSubreportBorders(CrystalDecisions.CrystalReports.Engine.ReportDocument document)
        {
            foreach (CrystalDecisions.CrystalReports.Engine.ReportObject reportObject in document.ReportDefinition.ReportObjects)
            {
                if (reportObject.Kind == ReportObjectKind.SubreportObject)
                {
                    reportObject.Border.BottomLineStyle = LineStyle.NoLine;
                    reportObject.Border.TopLineStyle = LineStyle.NoLine;
                    reportObject.Border.LeftLineStyle = LineStyle.NoLine;
                    reportObject.Border.RightLineStyle = LineStyle.NoLine;
                }
            }
            CrystalDecisions.CrystalReports.Engine.Subreports subReports = document.Subreports;
        }

        private void BtnConfigReport_Click(object sender, EventArgs e)
        {
            using (var formReportConfiguration = new FrmReportConfig())
            {
                var result = formReportConfiguration.ShowDialog();
                if (result == DialogResult.OK)
                {
                    reportLayoutConfiguration = formReportConfiguration.reportLayoutConfiguration;
                }
            }
        }
    }
}