﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CrystalReportsPlayground
{
    public partial class FrmSetParameters : Form
    {
        private ModelSection modelSectionSetParameters;

        public FrmSetParameters()
        {
            InitializeComponent();
        }

        public FrmSetParameters(ModelSection modelSection) : this()
        {
            this.modelSectionSetParameters = modelSection;
            if (modelSection.ParamName != null)
            {
                txtParameterName.Text = modelSection.ParamName.ToString();
            }
            if (modelSection.ParamValue != null)
            {
                txtParameterValue.Text = modelSection.ParamValue.ToString();
            }
        }       

        private void btnAddParameters_Click(object sender, EventArgs e)
        {
            modelSectionSetParameters.ParamName = txtParameterName.Text;
            modelSectionSetParameters.ParamValue = txtParameterValue.Text;
            Close();
        }
    }
}