﻿namespace CrystalReportsPlayground
{
    partial class FrmReportConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LbxAddSection = new System.Windows.Forms.ListBox();
            this.LbxRemoveSection = new System.Windows.Forms.ListBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbxAddSection
            // 
            this.LbxAddSection.FormattingEnabled = true;
            this.LbxAddSection.Location = new System.Drawing.Point(12, 53);
            this.LbxAddSection.Name = "lbxAddSection";
            this.LbxAddSection.Size = new System.Drawing.Size(170, 199);
            this.LbxAddSection.TabIndex = 0;
            this.LbxAddSection.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.LbxAllSection_MouseDoubleClick);
            // 
            // lbxRemoveSection
            // 
            this.LbxRemoveSection.FormattingEnabled = true;
            this.LbxRemoveSection.Location = new System.Drawing.Point(269, 53);
            this.LbxRemoveSection.Name = "lbxRemoveSection";
            this.LbxRemoveSection.Size = new System.Drawing.Size(170, 199);
            this.LbxRemoveSection.TabIndex = 1;
            this.LbxRemoveSection.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.LbxSelectedSection_MouseDoubleClick);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(360, 263);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(188, 100);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = ">>";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(188, 150);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(75, 23);
            this.btnRemove.TabIndex = 4;
            this.btnRemove.Text = "<<";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.BtnRemove_Click);
            // 
            // frmReportConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(455, 298);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.LbxRemoveSection);
            this.Controls.Add(this.LbxAddSection);
            this.Name = "frmReportConfig";
            this.Text = "Report Config";
            this.Load += new System.EventHandler(this.FrmReportConfig_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox LbxAddSection;
        private System.Windows.Forms.ListBox LbxRemoveSection;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnRemove;
    }
}