﻿namespace CrystalReportsPlayground
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.btnConfigReport = new System.Windows.Forms.Button();
            this.crvReportViewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 13);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(99, 35);
            this.button1.TabIndex = 0;
            this.button1.Text = "Generate Report";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.BtnGenerateReport_Click);
            // 
            // btnConfigReport
            // 
            this.btnConfigReport.Location = new System.Drawing.Point(132, 13);
            this.btnConfigReport.Name = "btnConfigReport";
            this.btnConfigReport.Size = new System.Drawing.Size(99, 35);
            this.btnConfigReport.TabIndex = 1;
            this.btnConfigReport.Text = "Config Report";
            this.btnConfigReport.UseVisualStyleBackColor = true;
            this.btnConfigReport.Click += new System.EventHandler(this.BtnConfigReport_Click);
            // 
            // crystalReportViewer1
            // 
            this.crvReportViewer.ActiveViewIndex = -1;
            this.crvReportViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crvReportViewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.crvReportViewer.Location = new System.Drawing.Point(12, 54);
            this.crvReportViewer.Name = "crystalReportViewer1";
            this.crvReportViewer.Size = new System.Drawing.Size(1215, 674);
            this.crvReportViewer.TabIndex = 2;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1239, 740);
            this.Controls.Add(this.crvReportViewer);
            this.Controls.Add(this.btnConfigReport);
            this.Controls.Add(this.button1);
            this.Name = "frmMain";
            this.Text = "Crystal Reports Playground";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnConfigReport;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crvReportViewer;
    }
}

