﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CrystalReportsPlayground
{
    public partial class FrmReportConfig : Form
    {
        public ReportLayoutConfig reportLayoutConfiguration = new ReportLayoutConfig();
        private ModelSection modelSectionSubReportOne;
        private ModelSection modelSectionSubReportTwo;
        private ModelSection modelSectionSubReportThree;

        public FrmReportConfig()
        {
            InitializeComponent();
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            reportLayoutConfiguration.ReportLayout = new List<ModelSection>();
            int counter = 0;
            foreach (var item in LbxRemoveSection.Items)
            {
                ModelSection modelSection = item as ModelSection;
                counter++;
                if (item.ToString() == modelSectionSubReportOne.SubreportName)
                {
                    reportLayoutConfiguration.ReportLayout.Add(new ModelSection { SubreportName = modelSectionSubReportOne.SubreportName, SectionIndex = counter, SubreportReportPath = modelSectionSubReportOne.SubreportReportPath, ParamName = modelSection.ParamName, ParamValue= modelSection.ParamValue });
                }
                if (item.ToString() == modelSectionSubReportTwo.SubreportName)
                {
                    reportLayoutConfiguration.ReportLayout.Add(new ModelSection { SubreportName = modelSectionSubReportTwo.SubreportName, SectionIndex = counter, SubreportReportPath = modelSectionSubReportTwo.SubreportReportPath, ParamName = modelSection.ParamName, ParamValue = modelSection.ParamValue });
                }
                if (item.ToString() == modelSectionSubReportThree.SubreportName)
                {
                    reportLayoutConfiguration.ReportLayout.Add(new ModelSection { SubreportName = modelSectionSubReportThree.SubreportName, SectionIndex = counter, SubreportReportPath = modelSectionSubReportThree.SubreportReportPath, ParamName = modelSection.ParamName, ParamValue = modelSection.ParamValue });
                }
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            if (LbxAddSection.SelectedIndex != -1)
            {
                foreach (var item in LbxAddSection.SelectedItems)
                {
                    ModelSection modelSection = item as ModelSection;
                    if (item.ToString() == modelSectionSubReportOne.SubreportName)
                    {
                        var modelSectionSubOne = new ModelSection { SubreportName = modelSectionSubReportOne.SubreportName, SubreportReportPath = modelSectionSubReportOne.SubreportReportPath, ParamName = modelSection.ParamName, ParamValue = modelSection.ParamValue };
                        LbxRemoveSection.Items.Add(modelSectionSubOne);
                    }
                    if (item.ToString() == modelSectionSubReportTwo.SubreportName)
                    {
                        var modelSectionSubTwo = new ModelSection { SubreportName = modelSectionSubReportTwo.SubreportName, SubreportReportPath = modelSectionSubReportTwo.SubreportReportPath, ParamName = modelSection.ParamName, ParamValue = modelSection.ParamValue };
                        LbxRemoveSection.Items.Add(modelSectionSubTwo);
                    }
                    if (item.ToString() == modelSectionSubReportThree.SubreportName)
                    {
                        var modelSectionSubTree = new ModelSection { SubreportName = modelSectionSubReportThree.SubreportName, SubreportReportPath = modelSectionSubReportThree.SubreportReportPath, ParamName = modelSection.ParamName, ParamValue = modelSection.ParamValue };
                        LbxRemoveSection.Items.Add(modelSectionSubTree);
                    }
                }
            }
        }

        private void BtnRemove_Click(object sender, EventArgs e)
        {
            if (LbxRemoveSection.SelectedIndex != -1)
            {
                for (int i = LbxRemoveSection.SelectedItems.Count - 1; i >= 0; i--)
                    LbxRemoveSection.Items.Remove(LbxRemoveSection.SelectedItems[i]);
            }
        }

        private void FrmReportConfig_Load(object sender, EventArgs e)
        {
            //Initialize basic Model Section models
            List<string> subReportPaths = Directory.GetFiles(Path.Combine(Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory())), "Sections"), "*.rpt")
                                                   .Select(path => Path.GetFullPath(path))
                                                   .ToList();

            foreach (var path in subReportPaths)
            {
                if (path.Contains("SectionReportTest1.rpt"))
                {
                    modelSectionSubReportOne = new ModelSection() { SubreportName = "SectionReportTest1", SubreportReportPath = path };
                }
                if (path.Contains("SectionReportTest2.rpt"))
                {
                    modelSectionSubReportTwo = new ModelSection() { SubreportName = "SectionReportTest2", SubreportReportPath = path };
                }
                if (path.Contains("SectionReportTest3.rpt"))
                {
                    modelSectionSubReportThree = new ModelSection() { SubreportName = "SectionReportTest3", SubreportReportPath = path };
                }
            }
            ReportLayoutConfig reportLayoutConfiguration = new ReportLayoutConfig();
            reportLayoutConfiguration.ReportLayout = new List<ModelSection>();

            reportLayoutConfiguration.ReportLayout.Add(modelSectionSubReportOne);
            reportLayoutConfiguration.ReportLayout.Add(modelSectionSubReportTwo);
            reportLayoutConfiguration.ReportLayout.Add(modelSectionSubReportThree);

            foreach (var item in reportLayoutConfiguration.ReportLayout)
            {
                LbxAddSection.Items.Add(item);
            }
        }

        private void LbxSelectedSection_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (LbxRemoveSection.SelectedItem != null)
            {
                ModelSection item = LbxRemoveSection.SelectedItem as ModelSection;
                FrmSetParameters setParametersForm = new FrmSetParameters(item);
                setParametersForm.ShowDialog();
            }
        }

        private void LbxAllSection_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (LbxAddSection.SelectedItem != null)
            {
                foreach (var item in LbxAddSection.SelectedItems)
                {
                    ModelSection modelSection = item as ModelSection;
                    if (item.ToString() == modelSectionSubReportOne.SubreportName)
                    {
                        var modelSectionSubOne = new ModelSection { SubreportName = modelSectionSubReportOne.SubreportName, SubreportReportPath = modelSectionSubReportOne.SubreportReportPath, ParamName = modelSection.ParamName, ParamValue = modelSection.ParamValue };
                        LbxRemoveSection.Items.Add(modelSectionSubOne);
                    }
                    if (item.ToString() == modelSectionSubReportTwo.SubreportName)
                    {
                        var modelSectionSubTwo = new ModelSection { SubreportName = modelSectionSubReportTwo.SubreportName, SubreportReportPath = modelSectionSubReportTwo.SubreportReportPath, ParamName = modelSection.ParamName, ParamValue = modelSection.ParamValue };
                        LbxRemoveSection.Items.Add(modelSectionSubTwo);
                    }
                    if (item.ToString() == modelSectionSubReportThree.SubreportName)
                    {
                        var modelSectionSubTree = new ModelSection { SubreportName = modelSectionSubReportThree.SubreportName, SubreportReportPath = modelSectionSubReportThree.SubreportReportPath, ParamName = modelSection.ParamName, ParamValue = modelSection.ParamValue };
                        LbxRemoveSection.Items.Add(modelSectionSubTree);
                    }
                }
            }
        }
    }
}