﻿namespace CrystalReportsPlayground
{
    public class ReportParameterList
    {
        public string Name { get; set; }

        public object Value { get; set; }

        public string SubreportName { get; set; }

        public string LocalName { get; set; }
    }
}